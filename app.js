require("dotenv").config();
const debug = require("debug")("calificaciones:root");
const morgan = require("morgan");
const express = require("express");

require("./db");

const app = express();

// Logger
app.use(morgan("dev"));

// ToDo: las rutas de cursos tienen que ir en las URLs /cursos/*
// ToDo: las rutas de calificaciones tienen que ir en las URLs /calificaciones/*

// Manejadores de errores
app.use((req, res, next) => {
  const error = new Error("La URL solicitada no existe");
  error.codigoStatus = 404;
  next(error);
});
app.use((err, req, res, next) => {
  const codigoStatus = err.codigoStatus || 500;
  const mensaje = codigoStatus === 500 ? "Ha ocurrido un error general" : err.message;
  console.log(mensaje);
  const respuesta = { mensaje };
  res.status(codigoStatus).json(respuesta);
});

// ToDo: haz que el servidor se levante y escuche en el puerto establecido en las variables de configuración

require("dotenv").config();
const debug = require("debug")("calificaciones:rutas:calificaciones");
const express = require("express");

const rutas = express.Router();

// ToDo: Si llega una petición GET a /calificaciones, se tiene que devolver un JSON con todas las calificaciones de la base de datos. Cada calificación debe mostrar el nombre del curso asociado.

// ToDo: Si llega una petición GET a /calificaciones/curso/XXXXX, se tiene que devolver un JSON con todas las calificaciones asociadas al curso XXXXX, sin mostrar el atributo "curso".

// ToDo: Si llega una petición POST a /calificaciones/crear, se tiene que crear una calificación en la base de datos y devolver un mensaje de OK.

// ToDo: Exporta las rutas

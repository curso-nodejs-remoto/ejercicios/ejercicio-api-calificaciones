require("dotenv").config();
const debug = require("debug")("calificaciones:modelos:calificacion");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

// ToDo: Crea el schema para la entidad Calificacion. Una calificación tiene los siguientes atributos:
// nombre (obligatorio), apellidos, curso (obligatorio y apunta a la entidad Curso) y nota (obligatorio, con valores entre 0 y 10)
// const CalificacionSchema = new Schema({ ... })

// ToDo: Crea el modelo para la entidad Calificacion a partir del esquema anterior. Haz que se relacione con la colección llamada "calificaciones".
// const Calificacion = mongoose.model(...)

// ToDo: Exporta el modelo que has creado

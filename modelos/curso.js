require("dotenv").config();
const debug = require("debug")("calificaciones:modelos:curso");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

// ToDo: Crea el schema para la entidad Curso. Un curso tiene sólo un nombre (obligatorio).
// const CursoSchema = new Schema({ ... })

// ToDo: Crea el modelo para la entidad Curso a partir del esquema anterior. Haz que se relacione con la colección llamada "cursos".
// const Curso = mongoose.model(...)

// ToDo: Exporta el modelo que has creado

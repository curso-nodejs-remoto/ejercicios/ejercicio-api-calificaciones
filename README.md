## Ejercicio - API Calificaciones

En este ejercicio crearás una API REST que interactuará con una base de datos MongoDB (https://www.mongodb.com/cloud/atlas), que almacena información sobre cursos y las calificaciones de los alumnos.

Descarga la interfaz gráfica para MongoDB Compass: https://www.mongodb.com/try/download/compass

1. En `app.js`, haz el ToDo del final del archivo.
2. En `db.js`, haz el ToDo y comprueba que funcionan tanto el servidor como la conexión a la base de datos.
3. Crea los esquemas y modelos correspondientes a la entidad Curso y a la entidad Calificación. Tienes los archivos con ToDos en la carpeta `modelos`.
4. Crea las rutas correspondientes a `/cursos`, en el archivo `rutas/cursos.js`.
5. En `app.js`, haz el ToDo correspondiente a las rutas de `/cursos`. Compruébalas con Postman.
6. Crea las rutas correspondientes a `/calificaciones`, en el archivo `rutas/calificaciones.js`.
7. En `app.js`, haz el ToDo correspondiente a las rutas de `/calificaciones`. Compruébalas con Postman.
